import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [searchValue, setSearchValue] = useState<string>('');

  useEffect(() => {
    const fetchData = async () => {
      if(searchValue!==''){
        const result = await backendClient.getUsersByTag(searchValue);
      }
      const result = await backendClient.getAllUsers();
      setUsers(result.data);
      setLoading(false);
    };

    fetchData();
  }, [searchValue]);

  const handleSearch = (searchValue: string) => {
    console.log(searchValue);
    setSearchValue(searchValue);
  }

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <span>
          <input onChange={(event) => (handleSearch(event.target.value))} />
        </span> 
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <span>
              {users.length
                ? users.map((user) => {
                    return <UserCard key={user.id} {...user} />;
                  })
                : null}
            </span>
          </div>
        )}
      </div>
    </div>
  );
};
