import people_data from '../data/people_data.json';

export class PeopleProcessing {
    getById(id: number) {
        return people_data.find((p) => p.id === id);
    }

    getByTag(tag: string) {
        return people_data.find((line) => line.gender ===  tag);
    }

    getAll() {
        return people_data;
    }
}
